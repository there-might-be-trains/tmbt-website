// two OSM nodes in a straight line
class PathSegment {
    constructor(start, end) {
        this.start = start;
        this.end = end;

        this.length = Math.sqrt(Math.pow(end[0] - start[0], 2) + Math.pow(end[1] - start[1], 2));
    }
}

// A path is a list of segments connecting two train stations. TODO: add a start time and end time instead of duration,
// so that the animation can be paused and resumed, and also to allow for pauses at stations.
class Path {
    constructor(segments, duration) {
        this.segments = segments;
        this.duration = duration;

        this.length = segments.reduce((a, b) => a + b.length, 0);
    }
}

// A route is a list of paths. Each path is a list of segments connecting two train stations
// and adjacent paths share a common train station.
class Route {
    constructor(paths) {
        this.paths = paths;
    }
}

// A train is a marker on the map that moves along a path
class Train {
    // Train on a map object with its path and a name
    constructor(map, path, name) {
        this.path = path;
        this.marker = L.marker([49.0068901, 8.4036527]).addTo(map)
              .bindPopup(name)
              .openPopup();
        this.counter = 0;
    }

    // update the position of the train marker
    update() {
        // simulate a train moving along a path, given the start and end points of a segment and a percentage
        // value. TODO: easing within the first few meters of a segment, but independent of the segment length.
        function simulateDriving(p1, p2, percentage) {
            //var f = -Math.sin(Math.PI / 2 + percentage * Math.PI) / 2 + 0.5;
            return [interpolate(p1[0], p2[0], percentage), interpolate(p1[1], p2[1], percentage)];
        }

        // linear interpolation between two values
        function interpolate(p1, p2, t) {
            return p1 + (p2 - p1) * t;
        }

        // increase the counter and reset it if it exceeds the path duration. TODO: use a timestamp instead of a counter
        this.counter += 1;
        if (this.counter >= this.path.duration) {
            this.counter = 0;
        }

        var progress = this.counter / this.path.duration;

        // calculate the segment that the train is currently on
        var segmentIndex = 0;
        var currentSegment = this.path.segments[segmentIndex];
        var coveredPathLength = 0;

        while (coveredPathLength + currentSegment.length <= progress * this.path.length) {
            coveredPathLength += currentSegment.length;
            currentSegment = this.path.segments[++segmentIndex];
        }

        // how much of the path does the current segment represent?
        var currentSegmentProgressRatio = currentSegment.length / this.path.length;

        // move the marker along the current segment
        this.marker.setLatLng(simulateDriving(currentSegment.start, currentSegment.end, (progress - (coveredPathLength / this.path.length)) / currentSegmentProgressRatio));
    }
}

var init = function () {
    var map = L.map('map').setView([49.0068901, 8.4036527], 13);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var kronenCircle = new Path([
        new PathSegment([49.0061175, 8.4102338], [49.0092637, 8.4102427]),
        new PathSegment([49.0092637, 8.4102427], [49.0088789, 8.4170266]),
        new PathSegment([49.0088789, 8.4170266], [49.0067412, 8.4129062]),
        new PathSegment([49.0067412, 8.4129062], [49.0059866, 8.4122273]),
        new PathSegment([49.0059866, 8.4122273], [49.0061175, 8.4102338])],
        10000);

    var schlossCircle = new Path([
        new PathSegment([49.0142390, 8.4005819], [49.0142884, 8.3999648]),
        new PathSegment([49.0142884, 8.3999648], [49.0153536, 8.3993156]),
        new PathSegment([49.0153536, 8.3993156], [49.0173575, 8.4026059]),
        new PathSegment([49.0173575, 8.4026059], [49.0171895, 8.4036770]),
        new PathSegment([49.0171895, 8.4036770], [49.0164165, 8.4039116]),
        new PathSegment([49.0164165, 8.4039116], [49.0145578, 8.4025659]),
        new PathSegment([49.0145578, 8.4025659], [49.0142390, 8.4005819])
    ], 15000);

    var paths = [
        kronenCircle,
        schlossCircle
    ]

    var testTrain = new Train(map, kronenCircle, "Nightliner");
    var testBahn = new Train(map, schlossCircle, "Schlossbahn");
    var trains = [testTrain, testBahn];

    var testAnimation = setInterval(animation, 10);

    function animation() {
        for (var i = 0; i < trains.length; i++) {
            trains[i].update();
        }
    }
}

window.onload = init